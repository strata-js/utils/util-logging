// ---------------------------------------------------------------------------------------------------------------------
// A basic interface for logging.
// ---------------------------------------------------------------------------------------------------------------------

/**
 * A very broad definition of a logging function.
 */
export type LogFunction = (message : string, ...extraArgs : unknown[]) => void;

/*
 * A very broad definition of a logger.
 */
export interface BasicLogger 
{
    trace : LogFunction;
    debug : LogFunction;
    info : LogFunction;
    warn : LogFunction;
    error : LogFunction;
    fatal : LogFunction;
    silent : LogFunction;
}

export const LogLevels = [ 'trace', 'debug', 'info', 'warn', 'error', 'fatal', 'silent' ];

export interface LoggerConfig 
{
    level ?: string;
    prettyPrint ?: boolean;
}

// ---------------------------------------------------------------------------------------------------------------------
