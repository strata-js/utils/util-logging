// ---------------------------------------------------------------------------------------------------------------------
// Logging Util
// ---------------------------------------------------------------------------------------------------------------------

import { BasicLogger, LoggerConfig } from '../interfaces/logger.js';
import { PinoLogger } from './logger.js';

// ---------------------------------------------------------------------------------------------------------------------

const loggerConfig : LoggerConfig = { level: 'debug', prettyPrint: true };
const loggers = new Map<string, BasicLogger>();

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Sets the config for all loggers created after this is called. (This will not affect current loggers.)
 *
 * @param config - the new configuration
 */
export function setConfig(config : LoggerConfig) : void
{
    Object.assign(loggerConfig, config);
}

/**
 * Gets a logger of the given name. If the name has already been used, it will return the same logging instance.
 *
 * @param name - the name of the logger.
 *
 * @returns Returns a logger instance.
 */
export function getLogger(name : string) : BasicLogger
{
    let logger = loggers.get(name);
    if(!logger)
    {
        logger = new PinoLogger(name, loggerConfig);
        loggers.set(name, logger as BasicLogger);
    }

    return logger as BasicLogger;
}

// ---------------------------------------------------------------------------------------------------------------------
