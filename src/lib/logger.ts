//----------------------------------------------------------------------------------------------------------------------
// SimpleLogger
//----------------------------------------------------------------------------------------------------------------------

import { pino } from 'pino';

// Interfaces
import { BasicLogger, LogLevels, LoggerConfig } from '../interfaces/logger.js';

// Utils
import { isModuleInstalled } from '../utils/modules.js';

//----------------------------------------------------------------------------------------------------------------------

const prettyPrintConfig = {
    errorProps: '*',
    levelFirst: false,
    messageKey: 'msg',
    timestampKey: 'time',
    translateTime: 'h:MM:ss TT',
    ignore: 'pid,hostname,moduleName,v',
};

//----------------------------------------------------------------------------------------------------------------------

export class PinoLogger implements BasicLogger
{
    #pino;
    readonly #config : LoggerConfig;
    public readonly name;

    constructor(name : string, config : LoggerConfig)
    {
        this.name = name;
        this.#config = config;

        if(this.#config.level && !LogLevels.includes(this.#config.level))
        {
            throw new TypeError(`Log level '${ this.#config.level }' is not a supported log level!`);
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    private _getPino() : pino.Logger
    {
        if(!this.#pino)
        {
            let transportConfig : pino.TransportSingleOptions | undefined = undefined;

            if(this.#config.prettyPrint && isModuleInstalled('pino-pretty'))
            {
                transportConfig = {
                    target: 'pino-pretty',
                    options: {
                        ...prettyPrintConfig,
                    },
                };
            }

            this.#pino = pino({
                name: this.name,
                level: this.#config.level ?? 'debug',
                transport: transportConfig,
            });
        }

        return this.#pino;
    }

    private _serializeArgs(logArgs : unknown[]) : string
    {
        return logArgs.map((arg : any) =>
        {
            if(arg)
            {
                if(arg.toJSON)
                {
                    return arg.toJSON();
                }

                if(arg instanceof Error)
                {
                    return arg.stack;
                }

                if(Object.prototype.toString.call(arg) === '[object Object]')
                {
                    try
                    {
                        return JSON.stringify(arg);
                    }
                    catch (_err)
                    {
                        return arg;
                    }
                }
            }

            return arg;
        }).join(' ');
    }

    //------------------------------------------------------------------------------------------------------------------

    public trace(...logArgs : unknown[]) : void
    {
        const logger = this._getPino();
        logger.trace(this._serializeArgs(logArgs));
    }

    public debug(...logArgs : unknown[]) : void
    {
        const logger = this._getPino();
        logger.debug(this._serializeArgs(logArgs));
    }

    public info(...logArgs : unknown[]) : void
    {
        const logger = this._getPino();
        logger.info(this._serializeArgs(logArgs));
    }

    public warn(...logArgs : unknown[]) : void
    {
        const logger = this._getPino();
        logger.warn(this._serializeArgs(logArgs));
    }

    public error(...logArgs : unknown[]) : void
    {
        const logger = this._getPino();
        logger.error(this._serializeArgs(logArgs));
    }

    public fatal(...logArgs : unknown[]) : void
    {
        const logger = this._getPino();
        logger.fatal(this._serializeArgs(logArgs));
    }

    public silent(...logArgs : unknown[]) : void
    {
        const logger = this._getPino();
        logger.silent(this._serializeArgs(logArgs));
    }
}

//----------------------------------------------------------------------------------------------------------------------
