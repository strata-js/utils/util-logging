# Logging Util

This is a basic logging library that wraps the [pino][] logging library. All this adds is some useful utility libraries,
as well as changing the default behavior of the logger to comply with the `console.*` api. (Meaning, when you pass 
multiple arguments, those objects are serialized and appending to the log message. In [pino][], those objects are added 
to the json logging payload.)

This project is under the Strata.js family, and it is included as a part of `@stratajs/strata`. We provide it as an 
external dependency, because logging is a commonly needed piece of functionality, and it's often desirable to have all 
code using the same logger.

You can find this logger exported from strata as:

```typescript
import { logging } from '@strata-js/strata';

// Create a logger
const logger = logging.loggerFor(module);
```

[pino]: https://getpino.io
[sst]: https://gitlab.com/stratajs/strata-service-tools

## Installation

This utility package is published to the public npm repository. Simply do:

```bash
// npm
$ npm add @strata-js/util-logging

// yarn
$ yarn add @strata-js/util-logging

```

## Usage

### Basic Usage

Using the logger is very simple:

```typescript
import { logging } from '@strata-js/util-logging';

const logger = logging.getLogger('myLogger');

// [0:00:00 AM] INFO (service): Hello World {"foo":"bar"}
logger.info('Hello', 'World', { foo: 'bar' });
```

### Advanced Usage

#### Configuration

The logger supports two possible pieces of configuration; `level` and `prettyPrint`. They are defined as follows:

* `level` - One of `trace`, `debug`, `info`, `warn`, `error`, or `critical`. This log level and above will be logged.
* `prettyPrint` - If true, will run through [pino-pretty][]. (Only takes effect with dev packages installed.)

[pino-pretty]: https://github.com/pinojs/pino-pretty

In order to set the configuration, call `setConfig()` and pass the new configuration. The configuration will take effect 
once an attempt is made to log to the logger. It will not update loggers that have already been logged to.

```typescript
import { logging } from '@strata-js/util-logging';

logging.setConfig({ prettyPrint: false });
const logger = logging.getLogger('myLogger');

// {"level":30,"time":1622349751673,"pid":10170,"hostname":"Hades","name":"service","msg":"Hello World {\"foo\":\"bar\"}"}
logger.info('Hello', 'World', { foo: 'bar' });
```

##### Disabling Logging

In order to disable logging, simply set the minimum logging level to `silent`:

```typescript
import { logging } from '@strata-js/util-logging';
logging.setConfig({ level: 'silent', prettyPrint: false });

// All logging from this point on will not show up

const logger = logging.getLogger('myLogger');
logger.error('This will not log!');
```

#### Logging Levels

The following levels are available:

* `trace`
* `debug`
* `info`
* `warn`
* `error`
* `critical`
* `silent`

These can be found on the `logger` object, as functions.

```typescript
import { logging } from '@strata-js/util-logging';

const logger = logging.getLogger('myLogger');

logger.trace('The `trace` level');
logger.debug('The `debug` level');
logger.info('The `info` level');
logger.warn('The `warn` level');
logger.error('The `error` level');
logger.critical('The `critical` level');
logger.silent('The `silent` level');    // This is a no-op and will not log anything.
```
